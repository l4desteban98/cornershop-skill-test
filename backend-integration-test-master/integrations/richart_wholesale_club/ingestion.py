import os
import re
import time
import numpy as np
import pandas as pd
import requests
import json

CONFIG_ERROR = False

try:
    with open("config.json", "r") as file:
        config = json.load(file)

    DEBUG = True
    MAX_PER_DAY = config['API']['MAX_PER_DAY']
    MAX_PER_HOUR = config['API']['MAX_PER_HOUR']
    ALLOWED_BRANCHES = config['GENERAL']['ALLOWED_BRANCHES']
    AMOUNT_SEND = 100 * len(ALLOWED_BRANCHES)
    MINUTES_CYCLE = 60 / (AMOUNT_SEND / MAX_PER_HOUR)
    SECONDS_CYCLE = 60 * MINUTES_CYCLE
    HOURS_CYCLE = MAX_PER_DAY / MAX_PER_HOUR
    HOURS_CYCLE_COUNTER = 0
    PACKAGE_REGEX = re.compile(r'[0-9]{1,9}[a-z,A-Z,\s]{1,3}')

    GRAND_TYPE = config['API']['GRAND_TYPE'] if not config['GENERAL']['USE_OS_ENVIRONMENT'] else os.environ.get(
        config['API']['GRAND_TYPE_ENVIRON_VAR'])

    CLIENT_ID = config['API']['CLIENT_ID'] if not config['GENERAL']['USE_OS_ENVIRONMENT'] else os.environ.get(
        config['API']['CLIENT_ID_ENVIRON_VAR'])

    CLIENT_SECRET = config['API']['CLIENT_SECRET'] if not config['GENERAL']['USE_OS_ENVIRONMENT'] else os.environ.get(
        config['API']['CLIENT_SECRET_ENVIRON_VAR'])

    HOST = config['API']['HOST']
    TOKEN = None
    TOKEN_HEADER = None
except Exception:
    CONFIG_ERROR = True

# pd.set_option('display.max_rows', 100)
# pd.set_option('display.max_columns', 100)
pd.options.mode.chained_assignment = None


def clean_html(text):
    if text and isinstance(text, str):
        return re.sub('<[^<]+?>', '', text)
    else:
        return text


def get_csv_files():
    if not DEBUG:
        import urllib.request
        PRODUCTS_CSV_URL = "https://cornershop-scrapers-evaluation.s3.amazonaws.com/public/PRODUCTS.csv"
        PRICES_CSV_URL = "https://cornershop-scrapers-evaluation.s3.amazonaws.com/public/PRICES-STOCK.csv"
        urllib.request.urlretrieve(PRODUCTS_CSV_URL, 'PRODUCTS.csv')
        urllib.request.urlretrieve(PRICES_CSV_URL, 'PRICES-STOCK.csv')
        return pd.read_csv('PRODUCTS.csv', delimiter="|"), pd.read_csv('PRICES-STOCK.csv', delimiter="|")
    else:
        return pd.read_csv('PRODUCTS.csv', delimiter="|"), pd.read_csv('PRICES-STOCK.csv', delimiter="|")


def process_csv_files():
    products_df, prices_stock_df = get_csv_files()

    branch_and_stock_filtered = prices_stock_df[prices_stock_df.STOCK > 0]
    branch_and_stock_filtered = branch_and_stock_filtered[branch_and_stock_filtered.BRANCH.isin(ALLOWED_BRANCHES)]

    # Just a temporal list to divide by the number of allowed branches
    # It must be divided in order to drop duplicates correctly
    branches_products = []
    # This will be the final result that is going to be appended to DataFrame
    branches_products_json = {}

    for branch in ALLOWED_BRANCHES:
        branches_products.append(branch_and_stock_filtered[branch_and_stock_filtered.BRANCH == branch])

    for branches_products_df in branches_products:
        # Drop SKU duplicates if they are
        if branches_products_df.SKU.duplicated().any():
            branches_products_df.drop_duplicates(subset="SKU", keep="first", inplace=True)

        branches_products_df.sort_values(by="PRICE", ascending=False, inplace=True)
        for bp in branches_products_df.head(100).values.tolist():
            if str(bp[0]) in branches_products_json.keys():
                branches_products_json[str(bp[0])].append(
                    {'branch': bp[1], 'stock': bp[3], 'price': bp[2]}
                )
            else:
                branches_products_json[str(bp[0])] = [
                    {'branch': bp[1], 'stock': bp[3], 'price': bp[2]}
                ]

    # Drop SKU duplicates if they are
    if products_df.SKU.duplicated().any():
        products_df.drop_duplicates(subset="SKU", keep="first", inplace=True)

    # Clean HTML from any text field
    for column_name in products_df.select_dtypes(include=['object']):
        products_df[column_name] = [clean_html(text) for text in products_df[column_name]]

    # Combine categories and sub-categories
    products_df['MERGED_CATEGORIES'] = products_df.apply(lambda row: str(row["CATEGORY"]).strip().lower() + "|" +
                                                                     str(row['SUB_CATEGORY']).strip().lower() + "|" +
                                                                     str(row['SUB_SUB_CATEGORY']).strip().lower(),
                                                         axis=1)

    products_df['PACKAGE'] = products_df.apply(lambda row:
                                               PACKAGE_REGEX.search(row['ITEM_DESCRIPTION']).group().strip()
                                               if PACKAGE_REGEX.search(row['ITEM_DESCRIPTION']) else "",
                                               axis=1)

    products_df = products_df[products_df.SKU.isin(branches_products_json.keys())]
    products_df['BRANCH_PRODUCTS'] = products_df.apply(lambda row: branches_products_json[str(row['SKU'])], axis=1)

    return products_df


if __name__ == "__main__":
    if not CONFIG_ERROR:
        try:
            TOKEN = requests.post(
                HOST + "/oauth/token?grant_type=%s&client_id=%s&client_secret=%s" % (
                GRAND_TYPE, CLIENT_ID, CLIENT_SECRET)
            )
            if TOKEN:
                TOKEN_HEADER = {
                    'token': str(TOKEN.json()['token_type']).capitalize() + " " + TOKEN.json()['access_token']}

                merchants = requests.get(HOST + "/api/merchants", headers=TOKEN_HEADER).json()['merchants']

                richards = {}

                beauty = {}

                for merchant in merchants:
                    if "Richard's" in merchant['name']:
                        richards = merchant

                    if "Beauty" in merchant['name']:
                        beauty = merchant

                # Richard's Update by PUT
                requests.put(HOST + "/api/merchants/%s" % richards['id'], headers=TOKEN_HEADER,
                             json={"id": richards['id'],
                                   "name": richards[
                                       'name'],
                                   "can_be_updated":
                                       richards[
                                           'can_be_updated'],
                                   "can_be_deleted":
                                       richards[
                                           'can_be_deleted'],
                                   "is_active": True})

                # Beauty remove by DELETE
                requests.delete(HOST + "/api/merchants/%s" % beauty['id'], headers=TOKEN_HEADER,
                                json={"id": beauty['id'],
                                      "name": beauty[
                                          'name'],
                                      "can_be_updated":
                                          beauty[
                                              'can_be_updated'],
                                      "can_be_deleted":
                                          beauty[
                                              'can_be_deleted'],
                                      "is_active": beauty[
                                          'is_active']})

                start_time = time.time()
                while HOURS_CYCLE_COUNTER < HOURS_CYCLE:
                    products = process_csv_files()
                    for index, product in products.iterrows():
                        response = requests.post(HOST + "/api/products", headers=TOKEN_HEADER, json={
                            "merchant_id": richards['id'],
                            "sku": str(product['SKU']),
                            "barcodes": [str(product['EAN'])],
                            "brand": product['BRAND_NAME'],
                            "name": product['ITEM_NAME'],
                            "description": product['ITEM_DESCRIPTION'],
                            "package": product['PACKAGE'],
                            "image_url": product['ITEM_IMG'],
                            "category": product['MERGED_CATEGORIES'],
                            "url": None,
                            "branch_products": product['BRANCH_PRODUCTS']
                        })
                    HOURS_CYCLE_COUNTER += 1
                    time.sleep(SECONDS_CYCLE)
            else:
                print("No se pudo obtener el token")
        except Exception:
            print("Error del sistema")
    else:
        print("Error de configuracion")
